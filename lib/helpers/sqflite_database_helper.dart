import 'dart:io';
// ignore: depend_on_referenced_packages
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';

class SqfliteDatabaseHelper {
  factory SqfliteDatabaseHelper() {
    return instance;
  }
  SqfliteDatabaseHelper._internal();
  static final SqfliteDatabaseHelper instance =
      SqfliteDatabaseHelper._internal();
  final _databaseName = 'assignment.db';
  final _databaseVersion = 1;

  // User table.
  String userTable = 'user_table';
  String id = 'id';
  String name = 'name';
  String userName = 'username';
  String email = 'email';
  String phoneNo = 'phone';
  String website = 'website';
  String countDown = 'countDown';

  // Address table.
  String addressTable = 'address_table';
  String addId = 'add_id';
  String street = 'street';
  String suite = 'suite';
  String city = 'city';
  String zipCode = 'zipcode';
  String userId = 'user_id';

  // Company table.
  String companyTable = 'company_table';
  String companyId = 'company_id';
  String companyName = 'name';
  String companyPhrase = 'catchPhrase';
  String companyBs = 'bs';

  // Geo location table
  String geoLocationTable = 'geo_table';
  String geoId = 'geo_id';
  String lat = 'lat';
  String lng = 'lng';

  Database? _database;
  Future<Database> get database async {
    if (_database != null) {
      return _database!;
    }
    return await _initDatabase();
  }

  dynamic _initDatabase() async {
    final Directory documentsDirectory =
        await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
          CREATE TABLE $userTable (
            $id INTEGER NOT NULL PRIMARY KEY,
            $name TEXT NOT NULL,
            $userName TEXT NOT NULL,
            $email TEXT NOT NULL,
            $phoneNo TEXT NOT NULL,
            $website TEXT NOT NULL,
            $countDown INTEGER NOT NULL)''');
    await db.execute('''
          CREATE TABLE $addressTable (
            $addId INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            $userId INTEGER NOT NULL,
            $street TEXT NOT NULL,
            $suite TEXT NOT NULL,
            $city TEXT NOT NULL,
            $zipCode TEXT,
            FOREIGN KEY($userId) REFERENCES $userTable($id))''');
    await db.execute('''
          CREATE TABLE $companyTable (
            $companyId INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            $userId INTEGER NOT NULL,
            $companyName TEXT NOT NULL,
            $companyPhrase TEXT,
            $companyBs TEXT,
            FOREIGN KEY($userId) REFERENCES $userTable($id))''');
    await db.execute('''
          CREATE TABLE $geoLocationTable (
            $geoId INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            $lat TEXT,
            $lng TEXT,
            $userId INTEGER NOT NULL,
            FOREIGN KEY($userId) REFERENCES $userTable($id))''');
  }

  Future<List<Map<String, dynamic>>> getAllData(String tableName) async {
    final Database db = await instance.database;
    return db.query(tableName);
  }

  Future<dynamic> searchRecord(
      {String? tableName, String? whereField, dynamic whereValue}) async {
    final Database db = await instance.database;
    return db
        .query(tableName!, where: '$whereField = ?', whereArgs: [whereValue]);
  }

  Future<int> insert(String tableName, Map<String, dynamic> row) async {
    final Database db = await instance.database;
    return db.insert(tableName, row);
  }

  Future<int> update(
      {String? tableName,
      String? whereField,
      dynamic whereValue,
      Map<String, dynamic>? row}) async {
    final Database db = await instance.database;
    return db.update(tableName!, row!,
        where: '$whereField = ?', whereArgs: [whereValue]);
  }

  Future<int> deleteByFilter(
      {String? tableName, String? whereField, String? whereValue}) async {
    final Database db = await instance.database;
    return db
        .delete(tableName!, where: "$whereField = ?", whereArgs: [whereValue]);
  }

  // delete all data form table.
  Future<int> deleteAll(String tablename) async {
    final Database db = await instance.database;
    return db.delete(tablename);
  }

  dynamic truncateDatabase() async {
    await deleteAll(userTable);
  }
}
