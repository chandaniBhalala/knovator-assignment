import 'package:assignment/models/api_response.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Request {
  // GET Request
  static Future<dynamic> get(
    String url, {
    Map<String, dynamic>? params,
    Map<String, String>? headers,
  }) async {
    var response = await http.get(Uri.parse(url),
        headers: _getHeaders(userHeaders: headers));
    return _processResponse(response);
  }

  // POST Request
  static Future<dynamic> post(
    String url, {
    Map<String, dynamic>? params,
    Map<String, String>? headers,
    dynamic body,
  }) async {
    var payload = json.encode(body);
    var response = await http.post(Uri.parse(url),
        body: payload, headers: _getHeaders(userHeaders: headers));
    return _processResponse(response);
  }

  static Map<String, String> _getHeaders({Map<String, String>? userHeaders}) {
    Map<String, String> headers = {
      "Content-type": "application/json",
    };
    if (userHeaders != null) {
      headers.addAll(userHeaders);
    }
    return headers;
  }

  static dynamic _processResponse(http.Response response) {
    return ApiResponse.fromJson(jsonDecode(response.body));
  }
}
