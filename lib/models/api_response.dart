import 'dart:convert';

ApiResponse apiResponseModelFromJson(String str) =>
    ApiResponse.fromJson(json.decode(str));

String apiResponseModelToJson(ApiResponse data) => json.encode(data.toJson());

class ApiResponse {
  ApiResponse({
    this.status = false,
    this.message,
    this.data,
  });

  bool status;
  String? message;
  dynamic data;

  factory ApiResponse.fromJson(dynamic json) => ApiResponse(
        status: true,
        message: 'Success',
        data: json,
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": data,
      };

  bool isOk() => status;
  bool isSuccessful() => status;
  bool hasError() => !status;
  bool hasData() {
    if (data == null) {
      return false;
    } else if (data is List) {
      if (data.length > 0) return true;
    } else if (data is Map) {
      if (data.length > 0) return true;
    }

    return false;
  }
}
