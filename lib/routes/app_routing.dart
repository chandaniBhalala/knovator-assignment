import 'package:assignment/views/profile.dart';
import 'package:get/get.dart';

List<GetPage> routes = [
  GetPage(name: '/profile', page: () => UserProfile()),
];
