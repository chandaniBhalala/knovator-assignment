import 'package:assignment/routes/app_routing.dart';
import 'package:assignment/utils/app_strings.dart';
import 'package:assignment/views/user_list.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: AppStrings.appTitle,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: UserList(),
      getPages: routes,
    );
  }
}
