import 'package:flutter/material.dart';

class AppColors {
  factory AppColors() {
    return _singleton;
  }

  AppColors._internal();
  static final AppColors _singleton = AppColors._internal();
  final Color? yellowColor = Colors.yellow[700];
  final Color? black = Colors.black;
  final Color? white = Colors.white;
  final Color black5c = const Color(0xff5c5e5c);
  final Color? grey = Colors.grey;
  final Color? darkGrey = Colors.grey[600];
  final Color? redColor = Colors.red;
  final Color? greenColor = Colors.green;
}
