class AppImages {
  //Image path
  static const String pngPath = 'assets/images/';

  //PNG images
  String get myOrders => '${pngPath}orders.png';
  String get memberShipCard => '${pngPath}member_card.png';
  String get deliveryAddress => '${pngPath}delivery_address.png';
  String get termsAndCondition => '${pngPath}terms_and_conditions.png';
  String get logout => '${pngPath}logout.png';
  String get noInternet => '${pngPath}no_internet.png';
  String get noRecords => '${pngPath}no_record.png';
  String get website => '${pngPath}website.png';
  String get company => '${pngPath}company.png';
}
