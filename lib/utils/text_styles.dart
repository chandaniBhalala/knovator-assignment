import 'package:assignment/utils/app_colors.dart';
import 'package:assignment/utils/app_fonts.dart';
import 'package:flutter/cupertino.dart';

class AppTextStyle {
  static const double pt9 = 9;
  static const double pt10 = 10;
  static const double pt11 = 11;
  static const double pt14 = 14;
  static const double pt12 = 12;
  static const double pt13 = 13;
  static const double pt15 = 15;
  static const double pt16 = 16;
  static const double pt17 = 17;
  static const double pt18 = 18;
  static const double pt19 = 19;
  static const double pt20 = 20;
  static const double pt21 = 21;
  static const double pt22 = 22;
  static const double pt24 = 24;
  static const double pt28 = 28;
  static const double pt34 = 34;

  static TextStyle _robotoBoldTextStyle(Color? fontColor, double? fontSize,
          FontWeight? fontWeight, TextDecoration? decoration) =>
      TextStyle(
        fontFamily: AppFonts.robotoBold,
        color: fontColor ?? AppColors().black,
        fontSize: fontSize ?? pt14,
        decoration: decoration,
        fontWeight: fontWeight ?? FontWeight.w700,
      );

  static TextStyle _robotoRegularTextStyle(Color? fontColor, double? fontSize,
          FontWeight? fontWeight, TextDecoration? decoration) =>
      TextStyle(
        fontFamily: AppFonts.robotoRegular,
        color: fontColor ?? AppColors().black,
        fontSize: fontSize ?? pt14,
        decoration: decoration,
        fontWeight: fontWeight ?? FontWeight.w400,
      );

  static TextStyle _robotoMediumTextStyle(Color? fontColor, double? fontSize,
          FontWeight? fontWeight, TextDecoration? decoration) =>
      TextStyle(
        fontFamily: AppFonts.robotoMedium,
        color: fontColor ?? AppColors().black,
        fontSize: fontSize ?? pt14,
        decoration: decoration,
        fontWeight: fontWeight ?? FontWeight.w500,
      );

  static TextStyle _robotoLightTextStyle(Color? fontColor, double? fontSize,
          FontWeight? fontWeight, TextDecoration? decoration) =>
      TextStyle(
        fontFamily: AppFonts.robotoLight,
        color: fontColor ?? AppColors().black,
        fontSize: fontSize ?? pt14,
        decoration: decoration,
        fontWeight: fontWeight ?? FontWeight.w400,
      );

  static TextStyle _robotoThinTextStyle(Color? fontColor, double? fontSize,
          FontWeight? fontWeight, TextDecoration? decoration) =>
      TextStyle(
        fontFamily: AppFonts.robotoThin,
        color: fontColor ?? AppColors().black,
        fontSize: fontSize,
        decoration: decoration,
        fontWeight: fontWeight,
      );

  static TextStyle robotoBoldText(
          {Color? color,
          double? fontSize,
          FontWeight? fontWeight,
          TextDecoration? decoration}) =>
      _robotoBoldTextStyle(color, fontSize, fontWeight, decoration);

  static TextStyle robotoRegularText(
          {Color? color,
          double? fontSize,
          FontWeight? fontWeight,
          TextDecoration? decoration}) =>
      _robotoRegularTextStyle(color, fontSize, fontWeight, decoration);

  static TextStyle robotoMediumText(
          {Color? color,
          double? fontSize,
          FontWeight? fontWeight,
          TextDecoration? decoration}) =>
      _robotoMediumTextStyle(color, fontSize, fontWeight, decoration);

  static TextStyle robotoLightText(
          {Color? color,
          double? fontSize,
          FontWeight? fontWeight,
          TextDecoration? decoration}) =>
      _robotoLightTextStyle(color, fontSize, fontWeight, decoration);

  static TextStyle robotoThinText(
          {Color? color,
          double? fontSize,
          FontWeight? fontWeight,
          TextDecoration? decoration}) =>
      _robotoThinTextStyle(color, fontSize, fontWeight, decoration);
}
