class AppFonts {
  factory AppFonts() {
    return _singleton;
  }

  AppFonts._internal();
  static final AppFonts _singleton = AppFonts._internal();
  static const String robotoBold = 'Roboto-Bold';
  static const String robotoMedium = 'Roboto-Medium';
  static const String robotoRegular = 'Roboto-Regular';
  static const String robotoLight = 'Roboto-Light';
  static const String robotoThin = 'Roboto-Thin';
}
