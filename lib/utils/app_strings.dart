class AppStrings {
  // User list
  static const String timeLeft = 'Time Left : ';
  static const String countDownComp = 'Countdown completed!';

  // Profile
  static const String myProfile = 'My Profile';
  static const String myAddress = 'My Address';
  static const String change = 'Change';
  static const String myOrders = 'My Orders';
  static const String myMembershipCard = 'My Membership Card';
  static const String myDeliveryAddress = 'My Delivery Address';
  static const String termsCondition = 'Terms & Conditions';
  static const String logout = 'Logout';
  
  // Others
  static const String appTitle = 'Assignment';
  static const String oops = 'Oops!';
  static const String noRecord = 'No Records found!';
  static const String error = 'Error!';
}
