import 'package:assignment/controllers/user_controller.dart';
import 'package:assignment/models/user_detail_model.dart';
import 'package:assignment/utils/app_colors.dart';
import 'package:assignment/utils/app_images.dart';
import 'package:assignment/utils/app_strings.dart';
import 'package:assignment/utils/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UserProfile extends StatelessWidget {
  UserProfile({Key? key}) : super(key: key);
  UserDetailModel? userDetail;
  final UserController userController = Get.find();
  @override
  Widget build(BuildContext context) {
    userDetail = Get.arguments['userDetail'];
    return Scaffold(
      backgroundColor: AppColors().white!,
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.black),
        elevation: 0,
        backgroundColor: AppColors().yellowColor!,
        title: Text(
          AppStrings.myProfile,
          style: AppTextStyle.robotoBoldText(fontSize: AppTextStyle.pt20),
        ),
        actions: [
          actionButton(
            Icon(
              Icons.search,
              color: AppColors().black5c,
            ),
          ),
          const SizedBox(width: 8),
          actionButton(
            Icon(
              Icons.calendar_today,
              color: AppColors().black5c,
            ),
          ),
          const SizedBox(width: 12),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          color: AppColors().yellowColor,
          padding: const EdgeInsets.only(top: 100.0),
          child: Stack(
            clipBehavior: Clip.none,
            children: [
              userDetailView(),
              profilePictureView(),
            ],
          ),
        ),
      ),
    );
  }

  Widget userDetailView() {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(50),
          topRight: Radius.circular(50),
        ),
      ),
      child: Column(
        children: [
          Padding(
              padding: const EdgeInsets.only(left: 210, top: 10.0),
              child: personalDetailView()),
          otherDetailView(),
          addressView(),
          optionsView(),
        ],
      ),
    );
  }

  Widget personalDetailView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Text(
                userDetail!.username!,
                style: AppTextStyle.robotoBoldText(fontSize: AppTextStyle.pt15),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 25.0),
              child: Container(
                padding: const EdgeInsets.all(3),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border:
                        Border.all(width: 2, color: AppColors().yellowColor!)),
                child: Icon(
                  Icons.edit,
                  color: AppColors().yellowColor!,
                  size: 16,
                ),
              ),
            ),
          ],
        ),
        const SizedBox(height: 8),
        Padding(
          padding: const EdgeInsets.only(right: 5.0, bottom: 8),
          child: Text(userDetail!.name!,
              style: AppTextStyle.robotoRegularText(
                  fontSize: AppTextStyle.pt13, color: AppColors().darkGrey)),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 5.0, bottom: 8),
          child: Text(userDetail!.email!,
              style: AppTextStyle.robotoRegularText(
                  fontSize: AppTextStyle.pt13, color: AppColors().darkGrey)),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 5.0),
          child: Text(userDetail!.phone!,
              style: AppTextStyle.robotoRegularText(
                  fontSize: AppTextStyle.pt13, color: AppColors().darkGrey)),
        ),
      ],
    );
  }

  Widget actionButton(icon) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        padding: const EdgeInsets.all(8),
        decoration: BoxDecoration(
            color: AppColors().white!.withOpacity(0.25),
            shape: BoxShape.circle),
        child: icon,
      ),
    );
  }

  Widget profilePictureView() {
    return Positioned(
      left: 30,
      top: -50,
      child: Container(
        height: 160,
        width: 160,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(color: AppColors().yellowColor!, width: 8)),
        child: Container(
          height: 150,
          width: 150,
          decoration: BoxDecoration(
              color: AppColors().white,
              shape: BoxShape.circle,
              image: const DecorationImage(
                  image: NetworkImage(
                      'https://www.shutterstock.com/image-photo/smiling-young-man-wearing-shirt-260nw-1309082077.jpg'),
                  fit: BoxFit.cover),
              border: Border.all(color: Colors.white, width: 5)),
        ),
      ),
    );
  }

  Widget addressView() {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0, bottom: 20, left: 24, right: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(AppStrings.myAddress,
                  style:
                      AppTextStyle.robotoBoldText(fontSize: AppTextStyle.pt15)),
              Padding(
                padding: const EdgeInsets.only(right: 20.0),
                child: Row(
                  children: [
                    Icon(
                      Icons.edit_note_rounded,
                      color: AppColors().yellowColor,
                    ),
                    Text(
                      AppStrings.change,
                      style: AppTextStyle.robotoBoldText(
                        fontSize: AppTextStyle.pt15,
                        color: AppColors().yellowColor,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Text(
                  '${userDetail!.address!.suite!}, ${userDetail!.address!.street!}, ${userDetail!.address!.city!} - ${userDetail!.address!.zipcode!}',
                  style: AppTextStyle.robotoRegularText(
                      fontSize: AppTextStyle.pt13, color: AppColors().darkGrey),
                ),
              ),
              IconButton(
                  onPressed: () {
                    userController.openMap(
                        double.parse(userDetail!.address!.geo!.lat!),
                        double.parse(userDetail!.address!.geo!.lng!));
                  },
                  icon: Icon(
                    Icons.location_on_outlined,
                    color: AppColors().yellowColor,
                  ))
            ],
          ),
        ],
      ),
    );
  }

  Widget otherDetailView() {
    return Padding(
      padding: const EdgeInsets.only(left: 24.0, top: 40),
      child: Column(
        children: [
          Row(
            children: [
              Image.asset(
                AppImages().company,
                height: 20,
                color: AppColors().yellowColor,
              ),
              const SizedBox(width: 15),
              Text(userDetail!.company!.name!)
            ],
          ),
          const SizedBox(height: 10),
          Row(
            children: [
              Image.asset(
                AppImages().website,
                height: 20,
                color: AppColors().yellowColor,
              ),
              const SizedBox(width: 15),
              Text(userDetail!.website!)
            ],
          )
        ],
      ),
    );
  }

  Widget optionsView() {
    return Column(
      children: [
        profileOptionTile(
            image: AppImages().myOrders, title: AppStrings.myOrders),
        profileOptionTile(
            image: AppImages().memberShipCard,
            title: AppStrings.myMembershipCard),
        profileOptionTile(
            image: AppImages().deliveryAddress,
            title: AppStrings.myDeliveryAddress),
        profileOptionTile(
            image: AppImages().termsAndCondition,
            title: AppStrings.termsCondition),
        profileOptionTile(image: AppImages().logout, title: AppStrings.logout),
        const SizedBox(height: 20),
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Text(
            'V.1.0',
            style: AppTextStyle.robotoRegularText(color: AppColors().darkGrey),
          ),
        ),
      ],
    );
  }

  Widget profileOptionTile({image, title}) {
    return Column(
      children: [
        Divider(color: AppColors().grey),
        Padding(
          padding:
              const EdgeInsets.only(left: 24.0, right: 24, top: 10, bottom: 10),
          child: Row(
            children: [
              Image.asset(
                image,
                height: 22,
              ),
              const SizedBox(width: 20),
              Text(
                title,
                style: AppTextStyle.robotoMediumText(),
              ),
              const Spacer(),
              Icon(
                Icons.chevron_right_sharp,
                color: AppColors().black5c,
              )
            ],
          ),
        )
      ],
    );
  }
}
