// import 'package:assignment/controllers/user_controller.dart';
// import 'package:assignment/models/user_detail_model.dart';
// import 'package:assignment/utils/app_colors.dart';
// import 'package:assignment/utils/app_strings.dart';
// import 'package:assignment/utils/text_styles.dart';
// import 'package:assignment/widgets/internet_connection_error.dart';
// import 'package:assignment/widgets/small_widget.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_countdown_timer/index.dart';
// import 'package:get/get.dart';

// class UserList extends StatelessWidget {
//   UserList({Key? key}) : super(key: key);

//   final UserController userController = Get.put(UserController());
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: SafeArea(
//         child: GetBuilder<UserController>(
//           builder: (controller) => !userController.isConnected
//               ? const NoInternetConnection()
//               : userController.isBusy
//                   ? const AppCircularProgress()
//                   : SingleChildScrollView(
//                       child: Padding(
//                         padding: const EdgeInsets.all(16.0),
//                         child: Column(
//                           children: List.generate(
//                             userController.userDetailList.length,
//                             (index) => userTile(
//                               controller.userDetailList[index],
//                             ),
//                           ),
//                         ),
//                       ),
//                     ),
//         ),
//       ),
//     );
//   }

//   Widget userTile(UserDetailModel userDetail) {
//     return CountdownTimer(
//         endTime: DateTime.now().millisecondsSinceEpoch +
//             Duration(seconds: userDetail.countDown!).inMilliseconds,
//         onEnd: () {
//           userController.onCountDownEnd(userDetail);
//         },
//         widgetBuilder: (_, CurrentRemainingTime? time) {
//           userController.updateCounterInDB(
//               time == null ? 0 : time.sec, userDetail.id);
//           userDetail.countDown = time == null ? 0 : time.sec;
//           return GestureDetector(
//             onTap: () {
//               Get.toNamed('/profile', arguments: {'userDetail': userDetail});
//             },
//             child: Padding(
//               padding: const EdgeInsets.symmetric(vertical: 8.0),
//               child: Container(
//                 padding: const EdgeInsets.all(8),
//                 decoration: BoxDecoration(
//                     color: userDetail.countDown == 0
//                         ? AppColors().redColor!.withOpacity(0.3)
//                         : AppColors().greenColor!.withOpacity(0.3),
//                     borderRadius: BorderRadius.circular(8),
//                     border: Border.all(
//                         color: userDetail.countDown == 0
//                             ? AppColors().redColor!
//                             : AppColors().greenColor!)),
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     Text(
//                       userDetail.name!,
//                       style: AppTextStyle.robotoBoldText(
//                           fontSize: AppTextStyle.pt16),
//                     ),
//                     const SizedBox(height: 10),
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       children: [
//                         Expanded(
//                           child: Text(
//                               '${userDetail.address!.suite!}, ${userDetail.address!.street!}, ${userDetail.address!.city!} - ${userDetail.address!.zipcode!}.',
//                               style: AppTextStyle.robotoRegularText(
//                                   fontSize: AppTextStyle.pt14)),
//                         ),
//                         const SizedBox(width: 10),
//                         Container(
//                           alignment: Alignment.centerRight,
//                           width: 150,
//                           child: Text(
//                             time == null
//                                 ? AppStrings.countDownComp
//                                 : '${AppStrings.timeLeft}${time.sec}s',
//                             style: AppTextStyle.robotoRegularText(),
//                           ),
//                         )
//                       ],
//                     )
//                   ],
//                 ),
//               ),
//             ),
//           );
//         });
//   }
// }

import 'package:assignment/controllers/user_controller.dart';
import 'package:assignment/models/user_detail_model.dart';
import 'package:assignment/utils/app_colors.dart';
import 'package:assignment/utils/app_strings.dart';
import 'package:assignment/utils/text_styles.dart';
import 'package:assignment/widgets/internet_connection_error.dart';
import 'package:assignment/widgets/small_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UserList extends StatelessWidget {
  UserList({Key? key}) : super(key: key);
  final UserController userController = Get.put(UserController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: GetBuilder<UserController>(
          builder: (_) => !userController.isConnected
              ? const NoInternetConnection()
              : userController.isBusy
                  ? const AppCircularProgress()
                  : SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          children: List.generate(
                            userController.userDetailList.length,
                            (index) =>
                                userTile(userController.userDetailList[index]),
                          ),
                        ),
                      ),
                    ),
        ),
      ),
    );
  }

  Widget userTile(UserDetailModel userDetail) {
    return GestureDetector(
      onTap: () {
        Get.toNamed('/profile', arguments: {'userDetail': userDetail});
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Container(
          padding: const EdgeInsets.all(8),
          decoration: BoxDecoration(
              color: userDetail.countDown == 0
                  ? AppColors().redColor!.withOpacity(0.3)
                  : AppColors().greenColor!.withOpacity(0.3),
              borderRadius: BorderRadius.circular(8),
              border: Border.all(
                  color: userDetail.countDown == 0
                      ? AppColors().redColor!
                      : AppColors().greenColor!)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                userDetail.name!,
                style: AppTextStyle.robotoBoldText(fontSize: AppTextStyle.pt16),
              ),
              const SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Text(
                        '${userDetail.address!.suite!}, ${userDetail.address!.street!}, ${userDetail.address!.city!} - ${userDetail.address!.zipcode!}.',
                        style: AppTextStyle.robotoRegularText(
                            fontSize: AppTextStyle.pt14)),
                  ),
                  const SizedBox(width: 10),
                  Container(
                    alignment: Alignment.centerRight,
                    width: 150,
                    child: Text(
                      userDetail.countDown == 0
                          ? AppStrings.countDownComp
                          : '${AppStrings.timeLeft}${userDetail.countDown}s',
                      style: AppTextStyle.robotoRegularText(),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
