import 'package:assignment/utils/app_colors.dart';
import 'package:assignment/utils/app_images.dart';
import 'package:assignment/utils/app_strings.dart';
import 'package:assignment/utils/text_styles.dart';
import 'package:flutter/material.dart';

class AppCircularProgress extends StatelessWidget {
  const AppCircularProgress({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: CircularProgressIndicator(
        color: AppColors().yellowColor,
        strokeWidth: 2.5,
      ),
    );
  }
}

class NoRecordsFound extends StatelessWidget {
  const NoRecordsFound({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(AppImages().noRecords, height: 150),
        const SizedBox(height: 20),
        Text(
          AppStrings.noRecord,
          style: AppTextStyle.robotoRegularText(
            color: AppColors().black,
            fontSize: AppTextStyle.pt12,
          ),
        )
      ],
    );
  }
}
