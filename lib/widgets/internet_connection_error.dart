import 'package:assignment/utils/app_colors.dart';
import 'package:assignment/utils/app_images.dart';
import 'package:assignment/utils/app_strings.dart';
import 'package:assignment/utils/text_styles.dart';
import 'package:flutter/material.dart';

class NoInternetConnection extends StatelessWidget {
  const NoInternetConnection(
      {Key? key,
      this.message = "You are not connected to internet!",
      this.action,
      this.actionLabel = "Retry"})
      : super(key: key);

  final String? message;
  final VoidCallback? action;
  final String actionLabel;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            AppImages().noInternet,
            height: 150,
          ),
          const SizedBox(height: 24.0),
          Text(AppStrings.oops,
              style: AppTextStyle.robotoBoldText(fontSize: AppTextStyle.pt20)),
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 32.0).copyWith(top: 8.0),
            child: Text(
              '$message',
              style: AppTextStyle.robotoMediumText(color: AppColors().black5c),
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }
}
