import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:get/get.dart';

class AppController extends GetxController {
  static AppController get to => Get.find();

  /// Observables
  RxBool setBusy = false.obs;
  final RxBool _isConnected = false.obs;

  /// Getters
  bool get isBusy => setBusy.value;
  bool get isConnected => _isConnected.value;

  @override
  void onInit() {
    super.onInit();
    checkConnection();
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        _isConnected(false);
        update();
      } else {
        _isConnected(true);
        update();
      }
    });
  }

  void checkConnection() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      _isConnected(false);
      update();
    } else {
      _isConnected(true);
      update();
    }
  }
}
