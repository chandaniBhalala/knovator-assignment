import 'dart:async';
import 'dart:math';
import 'package:assignment/config/config.dart';
import 'package:assignment/controllers/app_controller.dart';
import 'package:assignment/helpers/api_request.dart';
import 'package:assignment/helpers/sqflite_database_helper.dart';
import 'package:assignment/models/api_response.dart';
import 'package:assignment/models/user_detail_model.dart';
import 'package:assignment/utils/app_strings.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class UserController extends AppController {
  static UserController get to => Get.find();

  final dbHelper = SqfliteDatabaseHelper.instance;

  /// Observables
  final RxList<UserDetailModel> userList = <UserDetailModel>[].obs;

  /// Getters
  List<UserDetailModel> get userDetailList => userList;

  @override
  void onInit() {
    super.onInit();
    getUserDetail();
  }

  getUserDetail() async {
    setBusy(true);
    ApiResponse response = await Request.get(Config.getUserDetailList);
    if (response.hasError()) {
      Get.snackbar(AppStrings.error, 'Oops! Something wents wrong');
      setBusy(false);
      return;
    }
    if (response.hasData()) {
      await storeUserDetailInDB(response.data);
      await getUserFromDB();
      for (final user in userList) {
        _startCountdown(user);
      }
    }
    setBusy(false);
  }

  void _startCountdown(UserDetailModel user) {
    Timer.periodic(const Duration(seconds: 1), (timer) {
      if (user.countDown == 0) {
        _moveItemToEnd(user);
        timer.cancel();
        updateCounterInDB(user.countDown, user.id);
      } else {
        user.countDown = user.countDown! - 1;
        update();
        updateCounterInDB(user.countDown, user.id);
      }
    });
  }

  void _moveItemToEnd(UserDetailModel user) {
    userList.remove(user);
    userList.add(user);
    update();
  }

  storeUserDetailInDB(apiUserList) async {
    try {
      for (Map<String, dynamic> userData in apiUserList) {
        UserDetailModel user = UserDetailModel.fromJson(userData);
        Random random = Random();
        int countDown = random.nextInt(11) + 10;
        final Map<String, dynamic> userDetail = {
          dbHelper.id: user.id,
          dbHelper.name: user.name,
          dbHelper.userName: user.username,
          dbHelper.email: user.email,
          dbHelper.phoneNo: user.phone,
          dbHelper.website: user.website,
          dbHelper.countDown: countDown
        };
        final Map<String, dynamic> addressDetail = {
          dbHelper.userId: user.id,
          dbHelper.street: user.address!.street,
          dbHelper.suite: user.address!.suite,
          dbHelper.city: user.address!.city,
          dbHelper.zipCode: user.address!.zipcode,
        };
        final Map<String, dynamic> geoAddDetail = {
          dbHelper.lat: user.address!.geo!.lat,
          dbHelper.lng: user.address!.geo!.lng,
          dbHelper.userId: user.id,
        };
        final Map<String, dynamic> companyDetail = {
          dbHelper.userId: user.id,
          dbHelper.companyName: user.company!.name,
          dbHelper.companyPhrase: user.company!.catchPhrase,
          dbHelper.companyBs: user.company!.bs,
        };
        bool isExist = await checkIfUserExist(user);

        if (isExist) {
          await dbHelper.update(
              tableName: dbHelper.userTable,
              whereField: dbHelper.id,
              whereValue: user.id,
              row: userDetail);
          await dbHelper.update(
              tableName: dbHelper.addressTable,
              whereField: dbHelper.userId,
              whereValue: user.id,
              row: addressDetail);
          await dbHelper.update(
              tableName: dbHelper.geoLocationTable,
              whereField: dbHelper.userId,
              whereValue: user.id,
              row: geoAddDetail);
          await dbHelper.update(
              tableName: dbHelper.companyTable,
              whereField: dbHelper.userId,
              whereValue: user.id,
              row: companyDetail);
        } else {
          await dbHelper.insert(dbHelper.userTable, userDetail);
          await dbHelper.insert(dbHelper.addressTable, addressDetail);
          await dbHelper.insert(dbHelper.companyTable, companyDetail);
          await dbHelper.insert(dbHelper.geoLocationTable, geoAddDetail);
        }
      }
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
    }
  }

  Future<bool> checkIfUserExist(userData) async {
    try {
      final List<Map<String, dynamic>> isRecordExist =
          await dbHelper.searchRecord(
              tableName: dbHelper.userTable,
              whereField: dbHelper.id,
              whereValue: userData.id);
      if (isRecordExist.isEmpty) {
        return false;
      } else {
        return true;
      }
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
      return false;
    }
  }

  getUserFromDB() async {
    try {
      final List<Map<String, dynamic>> localAllUserList =
          await dbHelper.getAllData(dbHelper.userTable);
      for (Map<String, dynamic> userData in localAllUserList) {
        List<Map<String, dynamic>> address =
            await getAddressOfUser(userData[dbHelper.id]);
        Map<String, dynamic> userDetail = Map<String, dynamic>.from(userData);
        userDetail['address'] = address[0];
        List<Map<String, dynamic>> geoAddDetail =
            await getGeoAddressOfUser(userData[dbHelper.id]);
        userDetail['address']['geo'] = geoAddDetail[0];
        List<Map<String, dynamic>> companyDetail =
            await getCompanyOfUser(userData[dbHelper.id]);
        userDetail['company'] = companyDetail[0];
        UserDetailModel user = UserDetailModel.fromJson(userDetail);
        userList.add(user);
      }
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
    }
  }

  Future<List<Map<String, dynamic>>> getAddressOfUser(userId) async {
    final List<Map<String, dynamic>> localAddList = await dbHelper.searchRecord(
        tableName: dbHelper.addressTable,
        whereField: dbHelper.userId,
        whereValue: userId);
    List<Map<String, dynamic>> address = List<Map<String, dynamic>>.generate(
        localAddList.length,
        (index) => Map<String, dynamic>.from(localAddList[index]),
        growable: true);
    return address;
  }

  Future<List<Map<String, dynamic>>> getCompanyOfUser(userId) async {
    final List<Map<String, dynamic>> localCompanyList =
        await dbHelper.searchRecord(
            tableName: dbHelper.companyTable,
            whereField: dbHelper.userId,
            whereValue: userId);
    List<Map<String, dynamic>> company = List<Map<String, dynamic>>.generate(
        localCompanyList.length,
        (index) => Map<String, dynamic>.from(localCompanyList[index]),
        growable: true);
    return company;
  }

  Future<List<Map<String, dynamic>>> getGeoAddressOfUser(userId) async {
    final List<Map<String, dynamic>> localGeoAddList =
        await dbHelper.searchRecord(
            tableName: dbHelper.geoLocationTable,
            whereField: dbHelper.userId,
            whereValue: userId);
    List<Map<String, dynamic>> geoAddress = List<Map<String, dynamic>>.generate(
        localGeoAddList.length,
        (index) => Map<String, dynamic>.from(localGeoAddList[index]),
        growable: true);
    return geoAddress;
  }

  Future<void> openMap(double latitude, double longitude) async {
    String googleUrl =
        'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
    try {
      if (await launchUrl(Uri.parse(googleUrl),
          mode: LaunchMode.externalApplication)) {
      } else {
        throw 'Could not open the map.';
      }
    } catch (e) {
      if (kDebugMode) {
        print(e.toString());
      }
    }
  }

  updateCounterInDB(countDown, userId) async {
    await dbHelper.update(
        tableName: dbHelper.userTable,
        row: {dbHelper.countDown: countDown},
        whereField: dbHelper.id,
        whereValue: userId);
  }
}
